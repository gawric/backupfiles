﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.configModel
{
    public class iniSettingModel
    {
        public string in_Dir;
        public string out_Dir;
        public string ip_statistics;
        public string time_sync;
        public string active_sync;

        public string[] getArrayStrConf()
        {
            string[] array = new string[5];

            array[0] = in_Dir;
            array[1] = out_Dir;
            array[2] = ip_statistics;
            array[3] = time_sync;
            array[4] = active_sync;

            return array;
        }
    }
}
