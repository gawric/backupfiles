﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.SocketModel
{
   public class SocketClientModel
    {
        private Socket client;
        private string status;

        public Socket getClient()
        {
            return client;
        }

        public string getStatus()
        {
            return status;
        }

        public void setClient(Socket client)
        {
            this.client = client;
        }

        public void setStatus(string status)
        {
            this.status = status;
        }
    }
}
