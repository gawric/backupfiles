﻿using Microsoft.Practices.ServiceLocation;
using System;
using System.Collections.Generic;
using Prism.Modularity;
using Prism.Unity;
using ExampleModules;
using RootUtility;
using ExampleModules.Modules.MainSupportModule;
using ExampleModules.Controller.Config;
using ExampleModules.Controller.Config.support;
using ExampleModules.Controller.searchServerNetwork.support;
using ExampleModules.Controller.searchServerNetwork;
using ExampleModules.Controller.socketClient.client;
using ExampleModules.Controller.sqlite;
using ExampleModules.Controller.workerFiles;
using ExampleModules.Controller.CopyFiles;
using ExampleModules.Modules.SettingSupportModule;
using ExampleModules.Controller.syncMode;
using Microsoft.Practices.Unity;

namespace PrismeParseAsyncHH_Soft
{
    public class Bootstrapper : WinFormUnityBootstrapper
    {
        protected override System.Windows.Forms.Form CreateShell()
        {
            return ServiceLocator.Current.GetInstance<RootForm>();
        }


        protected override void InitializeShell()
        {
            // Add any custom initialization such as logger or other
            // components
            //todo...

            //Обновляют элементы SettingElemetn
            Container.RegisterType<SettingUpdateElements>(new ContainerControlledLifetimeManager());
            //обновляют MainPush
            Container.RegisterType<MainPushNotifi>(new ContainerControlledLifetimeManager());
            //обновляют MainElemetns
            Container.RegisterType<MainUpdateElements>(new ContainerControlledLifetimeManager());

            
            Container.RegisterType<checkConfigLoadFiles>(new ContainerControlledLifetimeManager());
            Container.RegisterType<checkConfigLoadAllRows>(new ContainerControlledLifetimeManager());
            Container.RegisterType<supportSearchServer>(new ContainerControlledLifetimeManager());
            Container.RegisterType<searchServer>(new ContainerControlledLifetimeManager());
            Container.RegisterType<AsyncClientSocket>(new ContainerControlledLifetimeManager());
            Container.RegisterType<SqliteController>(new ContainerControlledLifetimeManager());
            Container.RegisterType<CopyFilesRemotePath>(new ContainerControlledLifetimeManager());
            Container.RegisterType<WorkerFilesController>(new ContainerControlledLifetimeManager());
            Container.RegisterType<syncModeController>(new ContainerControlledLifetimeManager());
            Container.RegisterType<ConfigController>(new ContainerControlledLifetimeManager());
           
            Container.RegisterType<MainButton>(new ContainerControlledLifetimeManager());
            Container.RegisterType<SettingButton>(new ContainerControlledLifetimeManager());
            // Container.RegisterType<ConfigController>(new ContainerControlledLifetimeManager());


            // Container.TryResolve<ConfigController>();
            // Container.TryResolve<checkConfigLoadFiles>();
            // Container.TryResolve<checkConfigLoadAllRows>();
            // Container.TryResolve<supportSearchServer>();
            // Container.TryResolve<searchServer>();
            // Container.TryResolve<AsyncClientSocket>();
            // Container.TryResolve<SqliteController>();
            // Container.TryResolve<WorkerFilesController>();
            // Container.TryResolve<CopyFilesRemotePath>();
            // Container.TryResolve<syncModeController>();


            // Logger = this.CreateLogger();

            // if (Logger == null)
            // {
            //     throw new InvalidOperationException("The ILoggerFacade is required and cannot be null.");
            //  }
            //  else
            //  {
            // before shell loading this is not get triggered.
            //  if (Logger is LoggerUtility.Logger)
            //  {
            //      ((LoggerUtility.Logger)Logger).Initialize();
            //  }
            //  }


        }
        protected override void ConfigureModuleCatalog()
        {
            // Add required module initialization here
            // base.ConfigureModuleCatalog();
            List<Type> types = new List<Type>();

            types.Add(typeof(MainModule));
            types.Add(typeof(SettingModule));
            types.Add(typeof(HelpModule));



            foreach (var type in types)
            {
                ModuleCatalog.AddModule(new ModuleInfo()
                {
                    ModuleName = type.Name,
                    ModuleType = type.AssemblyQualifiedName,
                    InitializationMode = InitializationMode.WhenAvailable
                });
            }
        }

      //  protected override ILoggerFacade CreateLogger()
      //  {
      //      return new Logger();
      //  }

        protected override void ConfigureContainer()
        {
            base.ConfigureContainer();
        }
    }
}
