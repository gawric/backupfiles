﻿using System;
using System.Windows.Forms;
using Prism.Regions;
using Prism.Events;
using Microsoft.Practices.Unity;
using EventManager;
using ExampleModules.Controller;
using ExampleModules.Controller.Config;
using ExampleModules.Controller.Config.support;
using ExampleModules.Controller.sqlite;
using ExampleModules.Controller.syncMode;
using System.Collections.Generic;
using DataModel.configModel;

namespace PrismeParseAsyncHH_Soft
{
    public partial class RootForm : Form
    {
        IRegionManager m_regionManager;
        IUnityContainer m_container;
        IEventAggregator m_eventAggregator;
        ConfigController cfg;
        syncModeController syncMode;

        public RootForm(IUnityContainer container, IEventAggregator eventAggregator, IRegionManager regionManager)
        {
            InitializeComponent();
            m_regionManager = regionManager;
            m_container = container;
            m_eventAggregator = eventAggregator;
        }
      
        private IRegion CreateRegion(string regionName)
        {
            m_regionManager.Regions.Add(regionName, new Prism.Regions.Region());
            return m_regionManager.Regions[regionName];
        }
     
        private void menuPanel_Paint(object sender, PaintEventArgs e)
        {

        }

        //[0] filename - filename
        //[1] date
        public void pushInfo(string[] arr)
        {
            // задаем иконку всплывающей подсказки
            notifyIcon1.BalloonTipIcon = ToolTipIcon.Info;

            string filename = arr[0];
            string date = arr[1];

            // задаем текст подсказки
            notifyIcon1.BalloonTipText = filename+ " " +date;
            // устанавливаем зголовк
            notifyIcon1.BalloonTipTitle = "Синхронизация";
            // отображаем подсказку 12 секунд
            notifyIcon1.ShowBalloonTip(5);
        }

        //подписка для PushNotify
        public void Subscribe()
        {
            m_eventAggregator.GetEvent<PushNotificationEvent>().Subscribe(pushInfo);
        }

        private void RootForm_Load(object sender, EventArgs e)
        {
            //работа с конфигом
             cfg = m_container.Resolve<ConfigController>();

            //нужна только для кнопки save config что-бы остановить поиск
            checkConfigLoadFiles searchClient = m_container.Resolve<checkConfigLoadFiles>();

            //для базы
            SqliteController sql = m_container.Resolve<SqliteController>();
            sql.CreateBaseSqlite();
        
          
            //какой вид синхронизации будет использоваться
            //runtime или sync_file
            //syncModeController syncMode = m_container.Resolve<syncModeController>();

            Subscribe();
            startNotifyIcon();



        }

        //Прячет иконку в tree
        private void startNotifyIcon()
        {
            // делаем невидимой нашу иконку в трее
            notifyIcon1.Visible = false;

            // добавляем Эвент или событие по 2му клику мышки, 
            //вызывая функцию  notifyIcon1_MouseDoubleClick
            notifyIcon1.MouseDoubleClick += new MouseEventHandler(notifyIcon1_MouseDoubleClick);
            // добавляем событие на изменение окна
            this.Resize += new System.EventHandler(this.Form1_Resize);

        }

        private void Form1_Resize(object sender, EventArgs e)
        {
            // проверяем наше окно, и если оно было свернуто, делаем событие        
            if (WindowState == FormWindowState.Minimized)
            {
                // прячем наше окно из панели
                this.ShowInTaskbar = false;

                // делаем нашу иконку в трее активной
                notifyIcon1.Visible = true;
            }
        }


        private void notifyIcon1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            // делаем нашу иконку скрытой
            notifyIcon1.Visible = false;
            // возвращаем отображение окна в панели
            this.ShowInTaskbar = true;
            //разворачиваем окно
            WindowState = FormWindowState.Normal;
        }

        private void splitContainer1_Panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void tabPage2_Click(object sender, EventArgs e)
        {

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void notifyIcon2_MouseDoubleClick(object sender, MouseEventArgs e)
        {

        }

        private void выходToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void открытьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // делаем нашу иконку скрытой
            notifyIcon1.Visible = false;
            // возвращаем отображение окна в панели
            this.ShowInTaskbar = true;
            //разворачиваем окно
            WindowState = FormWindowState.Normal;
        }

        private void notifyIcon1_MouseDoubleClick_1(object sender, MouseEventArgs e)
        {

        }
    }
}
