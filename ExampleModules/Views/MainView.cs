﻿using System;

using System.Windows.Forms;
using Prism.Events;
using ExampleModules.ViewModel;
using Prism.Regions;
using EventManager;
using Microsoft.Practices.Unity;

using ExampleModules.Controller.sqlite;
using System.Data.SQLite;

namespace ExampleModules.Views
{
    public partial class MainView : UserControl, IWinFormDataContext
    {
        private IEventAggregator m_eventAggregator;
        private MainViewModel m_navModel;
        private SqliteController sql;


        public MainView(IUnityContainer container, MainViewModel mainTest, IEventAggregator eventAggregator)
        {
            m_navModel = mainTest;
            InitializeComponent();
            m_eventAggregator = eventAggregator;
            sql = container.Resolve<SqliteController>();
            Subscribe();
        }

        public object GetViewModel()
        {
            return m_navModel;
        }
        

        private void button1_Click(object sender, EventArgs e)
        {
            m_eventAggregator.GetEvent<MainButtonExitClickEvent>().Publish(sender.ToString());
        }

        private void MainView_Load(object sender, EventArgs e)
        {
            sql.startConnect();

            SQLiteDataReader reader = sql.sqlSelect();

            while (reader.Read())
            {
                string id = reader["id"].ToString();
                string dir = reader["dir"].ToString();
                string length = reader["length"].ToString();
                string dateTime = reader["dateTime"].ToString();
                string nameFiles = reader["nameFiles"].ToString();
                // Console.WriteLine(s2 + " " + s4);
                string final = dir + " " + dateTime;
                listBoxSyncFiles.Items.Add(final);
            }
        }

        //подписка для MainView
        public void Subscribe()
        {
            m_eventAggregator.GetEvent<MainUpdateViewElemetns>().Subscribe(FunctionUpdate);
            m_eventAggregator.GetEvent<MainListBoxChengeItems>().Subscribe(FunctionUpdateListBox);
        }

        //[0] - dir или FileName
        //[1] - lenght
        //[2] - dateTime
        //[3] - nameFiles
        private void FunctionUpdateListBox(string[] obj)
        {
            string dir = obj[0];
            long lenght = long.Parse(obj[1]); 
            string dateTime = obj[2];
            string nameFiles = obj[3];

            string final = dir + " " + dateTime;
            listBoxSyncFiles.BeginInvoke(new Action(() => listBoxSyncFiles.Items.Add(final)));
           

            sql.InsertRows(dir, lenght, dateTime, nameFiles);
        }
        ///[0] - name elemetns
        ///[1] - text
        private void FunctionUpdate(string[] obj)
        {
            string name = obj[0];
            string text = obj[1];


            Console.WriteLine("Cработала подписка Main " + name + " " + text);

            if (name.Equals("mainUpdateStatusConnServer"))
            {
                mainUpdateStatusConnServer.Text = text;
            }
            else if (name.Equals("visibleLastSaveLabel"))
            {
                //visibleLastSaveLabel.Text = text;
                visibleLastSaveLabel.BeginInvoke(new Action(() => visibleLastSaveLabel.Text = text));
            }
            else if (name.Equals("visibleStatusLabel"))
            {
               
                if(text.Equals("Работает"))
                {
                    visibleStatusLabel.BeginInvoke(new Action(()=> visibleStatusLabel.Text = text));
                    pictureStatus.BeginInvoke(new Action(() => pictureStatus.Image = Properties.Resources.greenV2));
                    
                }
                else
                {
                    visibleStatusLabel.BeginInvoke(new Action(() => visibleStatusLabel.Text = text));
                    pictureStatus.BeginInvoke(new Action(() => pictureStatus.Image = Properties.Resources.yellowV2));
                }
                   
            }
            else
            {
                Console.WriteLine("Main не смог найти такого поля >> " + name + " " + text);

            }

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            sql.ClearAsyncFilesTable();
            listBoxSyncFiles.Items.Clear();
        }
    }
}
