﻿namespace ExampleModules.Views
{
    partial class SettingView
    {
        /// <summary> 
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.settingLabelInFolder = new System.Windows.Forms.Label();
            this.settingButtonPictureIn = new System.Windows.Forms.PictureBox();
            this.settingTextBoxIn = new System.Windows.Forms.TextBox();
            this.settingButtonPictureOut = new System.Windows.Forms.PictureBox();
            this.settingTextBoxOut = new System.Windows.Forms.TextBox();
            this.settingLabelOutFolder = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.settingLabelDatePicker = new System.Windows.Forms.Label();
            this.settingDateTime = new System.Windows.Forms.DateTimePicker();
            this.label6 = new System.Windows.Forms.Label();
            this.updateSettingSelectSync = new System.Windows.Forms.CheckBox();
            this.labelsettingStatus = new System.Windows.Forms.Label();
            this.labelInfoSettingStatus = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.textBoxIpStatistics = new IPAddressControlLib.IPAddressControl();
            this.updateSettingLabelNameIP = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.settingUpdateSearchNetworkText = new System.Windows.Forms.Label();
            this.settingViewModelBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.updateSettingLabelInDirStatus = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.settingButtonPictureIn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.settingButtonPictureOut)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.settingViewModelBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // settingLabelInFolder
            // 
            this.settingLabelInFolder.AutoSize = true;
            this.settingLabelInFolder.Location = new System.Drawing.Point(61, 21);
            this.settingLabelInFolder.Name = "settingLabelInFolder";
            this.settingLabelInFolder.Size = new System.Drawing.Size(104, 13);
            this.settingLabelInFolder.TabIndex = 0;
            this.settingLabelInFolder.Text = "Откуда взять файл";
            // 
            // settingButtonPictureIn
            // 
            this.settingButtonPictureIn.Image = global::ExampleModules.Properties.Resources.folder;
            this.settingButtonPictureIn.Location = new System.Drawing.Point(31, 34);
            this.settingButtonPictureIn.Name = "settingButtonPictureIn";
            this.settingButtonPictureIn.Size = new System.Drawing.Size(24, 24);
            this.settingButtonPictureIn.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.settingButtonPictureIn.TabIndex = 1;
            this.settingButtonPictureIn.TabStop = false;
            this.settingButtonPictureIn.Click += new System.EventHandler(this.settingButtonPictureIn_Click);
            // 
            // settingTextBoxIn
            // 
            this.settingTextBoxIn.Location = new System.Drawing.Point(62, 37);
            this.settingTextBoxIn.Name = "settingTextBoxIn";
            this.settingTextBoxIn.Size = new System.Drawing.Size(163, 20);
            this.settingTextBoxIn.TabIndex = 2;
            // 
            // settingButtonPictureOut
            // 
            this.settingButtonPictureOut.Image = global::ExampleModules.Properties.Resources.folder;
            this.settingButtonPictureOut.Location = new System.Drawing.Point(31, 93);
            this.settingButtonPictureOut.Name = "settingButtonPictureOut";
            this.settingButtonPictureOut.Size = new System.Drawing.Size(24, 24);
            this.settingButtonPictureOut.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.settingButtonPictureOut.TabIndex = 3;
            this.settingButtonPictureOut.TabStop = false;
            this.settingButtonPictureOut.Click += new System.EventHandler(this.settingButtonPictureOut_Click);
            // 
            // settingTextBoxOut
            // 
            this.settingTextBoxOut.Location = new System.Drawing.Point(62, 96);
            this.settingTextBoxOut.Name = "settingTextBoxOut";
            this.settingTextBoxOut.Size = new System.Drawing.Size(163, 20);
            this.settingTextBoxOut.TabIndex = 4;
            // 
            // settingLabelOutFolder
            // 
            this.settingLabelOutFolder.AutoSize = true;
            this.settingLabelOutFolder.Location = new System.Drawing.Point(61, 80);
            this.settingLabelOutFolder.Name = "settingLabelOutFolder";
            this.settingLabelOutFolder.Size = new System.Drawing.Size(109, 13);
            this.settingLabelOutFolder.TabIndex = 5;
            this.settingLabelOutFolder.Text = "Куда вставить файл";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(160, 20);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(11, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "*";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(165, 77);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(11, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "*";
            this.label4.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // settingLabelDatePicker
            // 
            this.settingLabelDatePicker.AutoSize = true;
            this.settingLabelDatePicker.Location = new System.Drawing.Point(252, 21);
            this.settingLabelDatePicker.Name = "settingLabelDatePicker";
            this.settingLabelDatePicker.Size = new System.Drawing.Size(79, 13);
            this.settingLabelDatePicker.TabIndex = 8;
            this.settingLabelDatePicker.Text = "Время backup";
            // 
            // settingDateTime
            // 
            this.settingDateTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.settingDateTime.Location = new System.Drawing.Point(255, 37);
            this.settingDateTime.Name = "settingDateTime";
            this.settingDateTime.ShowUpDown = true;
            this.settingDateTime.Size = new System.Drawing.Size(84, 20);
            this.settingDateTime.TabIndex = 9;
            this.settingDateTime.ValueChanged += new System.EventHandler(this.settingDateTime_ValueChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(326, 19);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(11, 13);
            this.label6.TabIndex = 10;
            this.label6.Text = "*";
            // 
            // updateSettingSelectSync
            // 
            this.updateSettingSelectSync.AutoSize = true;
            this.updateSettingSelectSync.Location = new System.Drawing.Point(255, 64);
            this.updateSettingSelectSync.Name = "updateSettingSelectSync";
            this.updateSettingSelectSync.Size = new System.Drawing.Size(76, 17);
            this.updateSettingSelectSync.TabIndex = 11;
            this.updateSettingSelectSync.Text = "Time Sync";
            this.updateSettingSelectSync.UseVisualStyleBackColor = true;
            this.updateSettingSelectSync.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            this.updateSettingSelectSync.Click += new System.EventHandler(this.checkBox1_Click);
            // 
            // labelsettingStatus
            // 
            this.labelsettingStatus.AutoSize = true;
            this.labelsettingStatus.Location = new System.Drawing.Point(242, 117);
            this.labelsettingStatus.Name = "labelsettingStatus";
            this.labelsettingStatus.Size = new System.Drawing.Size(44, 13);
            this.labelsettingStatus.TabIndex = 12;
            this.labelsettingStatus.Text = "Cтатус:";
            // 
            // labelInfoSettingStatus
            // 
            this.labelInfoSettingStatus.AutoSize = true;
            this.labelInfoSettingStatus.Location = new System.Drawing.Point(282, 117);
            this.labelInfoSettingStatus.Name = "labelInfoSettingStatus";
            this.labelInfoSettingStatus.Size = new System.Drawing.Size(82, 13);
            this.labelInfoSettingStatus.TabIndex = 13;
            this.labelInfoSettingStatus.Text = "Конфиг.пустой";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(303, 204);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 14;
            this.button1.Text = "Сохранить";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // textBoxIpStatistics
            // 
            this.textBoxIpStatistics.AllowInternalTab = false;
            this.textBoxIpStatistics.AutoHeight = true;
            this.textBoxIpStatistics.BackColor = System.Drawing.SystemColors.Window;
            this.textBoxIpStatistics.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.textBoxIpStatistics.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.textBoxIpStatistics.Location = new System.Drawing.Point(62, 157);
            this.textBoxIpStatistics.MinimumSize = new System.Drawing.Size(87, 20);
            this.textBoxIpStatistics.Name = "textBoxIpStatistics";
            this.textBoxIpStatistics.ReadOnly = false;
            this.textBoxIpStatistics.Size = new System.Drawing.Size(155, 20);
            this.textBoxIpStatistics.TabIndex = 15;
            this.textBoxIpStatistics.Text = "...";
            // 
            // updateSettingLabelNameIP
            // 
            this.updateSettingLabelNameIP.AutoSize = true;
            this.updateSettingLabelNameIP.Location = new System.Drawing.Point(61, 141);
            this.updateSettingLabelNameIP.Name = "updateSettingLabelNameIP";
            this.updateSettingLabelNameIP.Size = new System.Drawing.Size(122, 13);
            this.updateSettingLabelNameIP.TabIndex = 16;
            this.updateSettingLabelNameIP.Text = "IP сервера статистики";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label10.Location = new System.Drawing.Point(31, 157);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(24, 17);
            this.label10.TabIndex = 17;
            this.label10.Text = "IP:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(64, 184);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(42, 13);
            this.label11.TabIndex = 18;
            this.label11.Text = "Поиск:";
            // 
            // settingUpdateSearchNetworkText
            // 
            this.settingUpdateSearchNetworkText.AutoSize = true;
            this.settingUpdateSearchNetworkText.Location = new System.Drawing.Point(107, 184);
            this.settingUpdateSearchNetworkText.Name = "settingUpdateSearchNetworkText";
            this.settingUpdateSearchNetworkText.Size = new System.Drawing.Size(63, 13);
            this.settingUpdateSearchNetworkText.TabIndex = 19;
            this.settingUpdateSearchNetworkText.Text = "Отключено";
            this.settingUpdateSearchNetworkText.Click += new System.EventHandler(this.settingUpdateSearchNetworkText_Click);
            // 
            // updateSettingLabelInDirStatus
            // 
            this.updateSettingLabelInDirStatus.AutoSize = true;
            this.updateSettingLabelInDirStatus.Location = new System.Drawing.Point(31, 219);
            this.updateSettingLabelInDirStatus.Name = "updateSettingLabelInDirStatus";
            this.updateSettingLabelInDirStatus.Size = new System.Drawing.Size(0, 13);
            this.updateSettingLabelInDirStatus.TabIndex = 20;
            // 
            // SettingView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.Controls.Add(this.updateSettingLabelInDirStatus);
            this.Controls.Add(this.settingUpdateSearchNetworkText);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.updateSettingLabelNameIP);
            this.Controls.Add(this.textBoxIpStatistics);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.labelInfoSettingStatus);
            this.Controls.Add(this.labelsettingStatus);
            this.Controls.Add(this.updateSettingSelectSync);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.settingDateTime);
            this.Controls.Add(this.settingLabelDatePicker);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.settingLabelOutFolder);
            this.Controls.Add(this.settingTextBoxOut);
            this.Controls.Add(this.settingButtonPictureOut);
            this.Controls.Add(this.settingTextBoxIn);
            this.Controls.Add(this.settingButtonPictureIn);
            this.Controls.Add(this.settingLabelInFolder);
            this.Name = "SettingView";
            this.Size = new System.Drawing.Size(390, 250);
            this.Load += new System.EventHandler(this.SettingView_Load);
            ((System.ComponentModel.ISupportInitialize)(this.settingButtonPictureIn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.settingButtonPictureOut)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.settingViewModelBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label settingLabelInFolder;
        private System.Windows.Forms.PictureBox settingButtonPictureIn;
        private System.Windows.Forms.TextBox settingTextBoxIn;
        private System.Windows.Forms.PictureBox settingButtonPictureOut;
        private System.Windows.Forms.TextBox settingTextBoxOut;
        private System.Windows.Forms.Label settingLabelOutFolder;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label settingLabelDatePicker;
        private System.Windows.Forms.DateTimePicker settingDateTime;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.CheckBox updateSettingSelectSync;
        private System.Windows.Forms.Label labelsettingStatus;
        private System.Windows.Forms.Label labelInfoSettingStatus;
        private System.Windows.Forms.Button button1;
        private IPAddressControlLib.IPAddressControl textBoxIpStatistics;
        private System.Windows.Forms.Label updateSettingLabelNameIP;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label settingUpdateSearchNetworkText;
        private System.Windows.Forms.BindingSource settingViewModelBindingSource;
        private System.Windows.Forms.Label updateSettingLabelInDirStatus;
    }
}
