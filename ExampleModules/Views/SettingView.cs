﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ExampleModules.ViewModel;
using Microsoft.Practices.Unity;
using Prism.Events;
using EventManager;
using DataModel.configModel;
using ExampleModules.Controller.Config;
using DataModel.statVariable;
using System.Globalization;
using ExampleModules.Controller.sqlite;

namespace ExampleModules.Views
{
    public partial class SettingView : UserControl
    {
        SettingViewModel SettingModel;
        IEventAggregator m_eventAggregator;
        IUnityContainer container;
        ConfigController cfg;
        OpenFileDialog fbd_in;
        OpenFileDialog fbd_out;

        public SettingView(IUnityContainer container, SettingViewModel SettingModel , IEventAggregator eventAggregator)
        {
            this.SettingModel = SettingModel;
            this.container = container;
            this.m_eventAggregator = eventAggregator;
            cfg = container.Resolve<ConfigController>();
            fbd_in = new OpenFileDialog();
            fbd_out = new OpenFileDialog();
       

            InitializeComponent();

            this.settingViewModelBindingSource.DataSource = SettingModel;

            Subscribe();
            //проверка, что конфиг загружен 
            cfg.CheckConfig();
        }

        private void SettingView_Load(object sender, EventArgs e)
        {
            settingDateTime.Format = DateTimePickerFormat.Custom;
            settingDateTime.CustomFormat = "HH:mm:ss";
        }

        //подписка для MainView
        public void Subscribe()
        {
            m_eventAggregator.GetEvent<SettingUpdateViewElemetns>().Subscribe(FunctionUpdate);
        }
        ///[0] - name elemetns
        ///[1] - text
        private void FunctionUpdate(string[] obj)
        {
            string name = obj[0];
            string text = obj[1];

            Console.WriteLine("Сработала подписка setting "+name+" "+text);

            if (name.Equals("settingUpdateSearchNetworkText"))
            {
                settingUpdateSearchNetworkText.Text = text;
            }
            else if (name.Equals("labelInfoSettingStatus"))
            {
                labelInfoSettingStatus.Text = text;
            }
            else if (name.Equals("updateSettingSelectSync"))
            {
                updateSettingSelectSync.Checked = bool.Parse(text);
            }
            else if (name.Equals("settingButtonPictureIn"))
            {
                fbd_in.FileName = text;
            }
            else if (name.Equals("settingButtonPictureOut"))
            {
                fbd_out.FileName = text;
            }
            else if (name.Equals("settingTextBoxIn"))
            {
                settingTextBoxIn.Text = text;
                
            }
            else if (name.Equals("settingTextBoxOut"))
            {
                //Console.WriteLine("Сработка " + text);
                settingTextBoxOut.Text = text;
            }
            else if (name.Equals("settingDateTime"))
            {
                string time = text;
                DateTime dateTime = DateTime.ParseExact(time, "HH:mm:ss",
                                        CultureInfo.InvariantCulture);
                settingDateTime.Value = dateTime;
            }
            else if (name.Equals("textBoxIpStatistics"))
            {
                textBoxIpStatistics.Text = text;
            }
            else
            {
                    Console.WriteLine("Setting не смог найти такого поля >> " + name + " " + text);
            }

        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
           // updateSettingLabelInDirStatus.Text = "Сохраните настройки для изменения";
           // updateSettingLabelInDirStatus.ForeColor = Color.IndianRed;
        }

        private void checkBox1_Click(object sender, EventArgs e)
        {
             updateSettingLabelInDirStatus.Text = "Сохраните настройки для изменения";
             updateSettingLabelInDirStatus.ForeColor = Color.IndianRed;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //m_eventAggregator.GetEvent<SettingButtonCLickEvent>().Publish(sender.ToString());
           // Console.WriteLine("Нажата кнопка");
            //получаем загруженный конфиг
            iniSettingModel s = cfg.getConfig();

            string in_text = settingTextBoxIn.Text;

            if (s.in_Dir.Equals(in_text))
            {

            }
            else
            {

                //следим за изменениями в файле
               //worker.ChangeWatchFile(in_text);
                // Console.WriteLine("Реквизиты inTextBox изменились "+ in_text);
            }

            //сохраняем параметры в конфиг
            saveConfigSetting(cfg);

            updateSettingLabelInDirStatus.Text = "";
            updateSettingLabelInDirStatus.ForeColor = Color.IndianRed;

            //перезагружаем конфиг и перезапускаем syncMode
            cfg.CheckConfig();


        }

        private void settingUpdateSearchNetworkText_Click(object sender, EventArgs e)
        {

        }

        private void saveConfigSetting(ConfigController cfg)
        {
            //откуда
            string in_dir = settingTextBoxIn.Text;
            //куда
            string out_dir = settingTextBoxOut.Text;
            //статистика
            string ip_statistics = textBoxIpStatistics.Text;
            //когда делать синхронизацию
            string time = settingDateTime.Text;

            bool bool_select_sync = updateSettingSelectSync.Checked;

            string select_sync;

            //true - selectSync
            //false - runtimeSync
            if (bool_select_sync)
            {
                // syncMode.ChangeTimeSync();

                select_sync = variable.select_sync;
            }
            else
            {
                // syncMode.ChangeWatchFile(in_dir);

                select_sync = variable.runtime_sync;
            }

            string status = cfg.saveConfig(in_dir, out_dir, ip_statistics, time, select_sync);

            updateSettingSaveText(status);
        }

        private void createConfig(ConfigController cfg)
        {
            cfg.createConfig("", "", "", "", "");
        }

        //вставляем текст в OpenFilesDialog и остальные поля
        public void insertSettingText(iniSettingModel model)
        {
            string out_dir = model.out_Dir;
            string in_dir = model.in_Dir;
            string ip_stat_server = model.ip_statistics;
            string time_sync = model.time_sync;

            fbd_out.FileName = out_dir;
            fbd_in.FileName = in_dir;
            //откуда
            settingTextBoxIn.Text = in_dir;
            //куда
            settingTextBoxOut.Text = out_dir;
            //статистика
            textBoxIpStatistics.Text = ip_stat_server;
            //время
            settingDateTime.Text = time_sync;
        }

        public void updateSettingSaveText(string status)
        {
           

            if (status.Equals("OK"))
            {
                labelInfoSettingStatus.Text = "конф.обновлен";
                //MessageBox.Show("Данные успешно обновлены");
            }
            else
            {
                labelInfoSettingStatus.Text = "Ошибка";
                MessageBox.Show(status);
            }
        }

        private void settingButtonPictureIn_Click(object sender, EventArgs e)
        {
            if (fbd_in.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                updateSettingLabelInDirStatus.Text = "Сохраните настройки для изменения";
                updateSettingLabelInDirStatus.ForeColor = Color.IndianRed;

                settingTextBoxIn.Text = fbd_in.FileName;

            }
        }

        private void settingButtonPictureOut_Click(object sender, EventArgs e)
        {
            if (fbd_out.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                updateSettingLabelInDirStatus.Text = "Сохраните настройки для изменения";
                updateSettingLabelInDirStatus.ForeColor = Color.IndianRed;

                settingTextBoxOut.Text = fbd_out.FileName;

            }
        }

        private void settingDateTime_ValueChanged(object sender, EventArgs e)
        {

        }
    }
}
