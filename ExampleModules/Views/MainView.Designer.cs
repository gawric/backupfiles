﻿namespace ExampleModules.Views
{
    partial class MainView
    {
        /// <summary> 
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.status_Label = new System.Windows.Forms.Label();
            this.visibleStatusLabel = new System.Windows.Forms.Label();
            this.statusLabelLastSave = new System.Windows.Forms.Label();
            this.visibleLastSaveLabel = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.mainUpdateStatusConnServer = new System.Windows.Forms.Label();
            this.labelLastFiles = new System.Windows.Forms.Label();
            this.ButtonExit = new System.Windows.Forms.Button();
            this.listBoxSyncFiles = new System.Windows.Forms.ListBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureStatus = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureStatus)).BeginInit();
            this.SuspendLayout();
            // 
            // status_Label
            // 
            this.status_Label.AutoSize = true;
            this.status_Label.Location = new System.Drawing.Point(29, 19);
            this.status_Label.Name = "status_Label";
            this.status_Label.Size = new System.Drawing.Size(41, 13);
            this.status_Label.TabIndex = 0;
            this.status_Label.Text = "Статус";
            // 
            // visibleStatusLabel
            // 
            this.visibleStatusLabel.AutoSize = true;
            this.visibleStatusLabel.Location = new System.Drawing.Point(51, 46);
            this.visibleStatusLabel.Name = "visibleStatusLabel";
            this.visibleStatusLabel.Size = new System.Drawing.Size(76, 13);
            this.visibleStatusLabel.TabIndex = 2;
            this.visibleStatusLabel.Text = "Подключение";
            // 
            // statusLabelLastSave
            // 
            this.statusLabelLastSave.AutoSize = true;
            this.statusLabelLastSave.Location = new System.Drawing.Point(34, 93);
            this.statusLabelLastSave.Name = "statusLabelLastSave";
            this.statusLabelLastSave.Size = new System.Drawing.Size(125, 13);
            this.statusLabelLastSave.TabIndex = 3;
            this.statusLabelLastSave.Text = "Последнее сохранение";
            // 
            // visibleLastSaveLabel
            // 
            this.visibleLastSaveLabel.AutoSize = true;
            this.visibleLastSaveLabel.Location = new System.Drawing.Point(36, 108);
            this.visibleLastSaveLabel.Name = "visibleLastSaveLabel";
            this.visibleLastSaveLabel.Size = new System.Drawing.Size(98, 13);
            this.visibleLastSaveLabel.TabIndex = 4;
            this.visibleLastSaveLabel.Text = "xx.xx.xxxx 00:00:00";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(241, 20);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(86, 13);
            this.label5.TabIndex = 5;
            this.label5.Text = "Статус сервера";
            // 
            // mainUpdateStatusConnServer
            // 
            this.mainUpdateStatusConnServer.AutoSize = true;
            this.mainUpdateStatusConnServer.ForeColor = System.Drawing.Color.Maroon;
            this.mainUpdateStatusConnServer.Location = new System.Drawing.Point(272, 48);
            this.mainUpdateStatusConnServer.Name = "mainUpdateStatusConnServer";
            this.mainUpdateStatusConnServer.Size = new System.Drawing.Size(96, 13);
            this.mainUpdateStatusConnServer.TabIndex = 6;
            this.mainUpdateStatusConnServer.Text = "Нет подключения";
            // 
            // labelLastFiles
            // 
            this.labelLastFiles.AutoSize = true;
            this.labelLastFiles.Location = new System.Drawing.Point(34, 143);
            this.labelLastFiles.Name = "labelLastFiles";
            this.labelLastFiles.Size = new System.Drawing.Size(143, 13);
            this.labelLastFiles.TabIndex = 8;
            this.labelLastFiles.Text = "Последние синхронизации";
            // 
            // ButtonExit
            // 
            this.ButtonExit.Location = new System.Drawing.Point(303, 204);
            this.ButtonExit.Name = "ButtonExit";
            this.ButtonExit.Size = new System.Drawing.Size(75, 23);
            this.ButtonExit.TabIndex = 10;
            this.ButtonExit.Text = "Выход";
            this.ButtonExit.UseVisualStyleBackColor = true;
            this.ButtonExit.Click += new System.EventHandler(this.button1_Click);
            // 
            // listBoxSyncFiles
            // 
            this.listBoxSyncFiles.FormattingEnabled = true;
            this.listBoxSyncFiles.HorizontalScrollbar = true;
            this.listBoxSyncFiles.Location = new System.Drawing.Point(37, 159);
            this.listBoxSyncFiles.Name = "listBoxSyncFiles";
            this.listBoxSyncFiles.Size = new System.Drawing.Size(227, 69);
            this.listBoxSyncFiles.TabIndex = 11;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::ExampleModules.Properties.Resources.iconsClear20;
            this.pictureBox1.Location = new System.Drawing.Point(270, 159);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(20, 20);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox1.TabIndex = 12;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::ExampleModules.Properties.Resources.server20;
            this.pictureBox2.Location = new System.Drawing.Point(244, 45);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(20, 20);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox2.TabIndex = 7;
            this.pictureBox2.TabStop = false;
            // 
            // pictureStatus
            // 
            this.pictureStatus.Image = global::ExampleModules.Properties.Resources.yellowV2;
            this.pictureStatus.Location = new System.Drawing.Point(32, 44);
            this.pictureStatus.Name = "pictureStatus";
            this.pictureStatus.Size = new System.Drawing.Size(16, 16);
            this.pictureStatus.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureStatus.TabIndex = 1;
            this.pictureStatus.TabStop = false;
            // 
            // MainView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.listBoxSyncFiles);
            this.Controls.Add(this.ButtonExit);
            this.Controls.Add(this.labelLastFiles);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.mainUpdateStatusConnServer);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.visibleLastSaveLabel);
            this.Controls.Add(this.statusLabelLastSave);
            this.Controls.Add(this.visibleStatusLabel);
            this.Controls.Add(this.pictureStatus);
            this.Controls.Add(this.status_Label);
            this.Name = "MainView";
            this.Size = new System.Drawing.Size(401, 240);
            this.Load += new System.EventHandler(this.MainView_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureStatus)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label status_Label;
        private System.Windows.Forms.PictureBox pictureStatus;
        private System.Windows.Forms.Label visibleStatusLabel;
        private System.Windows.Forms.Label statusLabelLastSave;
        private System.Windows.Forms.Label visibleLastSaveLabel;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label mainUpdateStatusConnServer;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label labelLastFiles;
        private System.Windows.Forms.Button ButtonExit;
        private System.Windows.Forms.ListBox listBoxSyncFiles;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}
