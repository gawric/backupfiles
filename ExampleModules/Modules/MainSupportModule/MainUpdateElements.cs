﻿using EventManager;
using ExampleModules.Views;
using Microsoft.Practices.Unity;
using Prism.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExampleModules.Modules.MainSupportModule
{
    public class MainUpdateElements
    {

       private IEventAggregator m_eventAggregator;
       private IUnityContainer container;
       private string[] Info = new string[2];


        public MainUpdateElements(IUnityContainer container, IEventAggregator m_eventAggregator)
        {
            this.m_eventAggregator = m_eventAggregator;
         
        }

        public void UpdateMainElemetns(string elemetns, string text)
        {
            Info[0] = elemetns;
            Info[1] = text;

            m_eventAggregator.GetEvent<MainUpdateViewElemetns>().Publish(Info);
        }


    }
}
