﻿using EventManager;
using Microsoft.Practices.Unity;
using Prism.Events;
using System.Windows.Forms;

namespace ExampleModules.Modules.MainSupportModule
{
    public class MainButton
    {
        IEventAggregator m_eventAggregator;
        IUnityContainer container;

        public MainButton(IUnityContainer container,  IEventAggregator m_eventAggregator)
        {
            this.m_eventAggregator = m_eventAggregator;
            Subscribe();
        }

        //подписка для MainView
        public void Subscribe()
        {
            m_eventAggregator.GetEvent<MainButtonExitClickEvent>().Subscribe(FunctionExit);
        }

        public void FunctionExit(string obj)
        {
            Application.Exit();
        }
    }

   
}
