﻿using EventManager;
using Microsoft.Practices.Unity;
using Prism.Events;


namespace ExampleModules.Modules.MainSupportModule
{
    public class MainPushNotifi
    {
        
            private IEventAggregator m_eventAggregator;
            private IUnityContainer container;
            private string[] Info = new string[2];


            public MainPushNotifi(IUnityContainer container, IEventAggregator m_eventAggregator)
            {
                this.m_eventAggregator = m_eventAggregator;

            }

            public void pushNotification(string filename, string date)
            {
                Info[0] = filename;
                Info[1] = date;

                m_eventAggregator.GetEvent<PushNotificationEvent>().Publish(Info);
            }


        
    }
}
