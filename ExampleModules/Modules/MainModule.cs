﻿using EventManager;
using ExampleModules;
using ExampleModules.Modules.MainSupportModule;
using ExampleModules.Views;
using Microsoft.Practices.Unity;
using Prism.Events;
using Prism.Modularity;
using Prism.Regions;
using System;
using System.Windows.Forms;

namespace ExampleModules
{
    public class MainModule : IModule
    {
        IUnityContainer m_unityContainer;
        IRegionManager m_regionManager;
        IEventAggregator m_eventAggregator;
        MainView m_subjectView;
        MainButton Mbutton;

        public MainModule(IUnityContainer container, IRegionManager regionManager , IEventAggregator m_eventAggregator)
        {
            m_unityContainer = container;
            m_regionManager = regionManager;
            this.m_eventAggregator = m_eventAggregator;
            m_unityContainer = container;
            
        }

        public void Initialize()
        {
            AddMainView();
           
        }

        private void AddMainView()
        {
            MainView view = m_unityContainer.Resolve<MainView>();
            IRegion reg = m_regionManager.Regions["MainRegion"];
            Panel panel = reg.RegionHost as Panel;
            reg.Add(view, "MainView");
            reg.Activate(view);
            view.Dock = DockStyle.Top;
            panel.Height = view.Height;
        }

      






    }
}