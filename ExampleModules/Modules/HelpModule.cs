﻿using ExampleModules.Views;
using Microsoft.Practices.Unity;
using Prism.Modularity;
using Prism.Regions;
using RootUtility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ExampleModules
{
    public class HelpModule : IModule
    {
        IUnityContainer m_unityContainer;
        IRegionManager m_regionManager;
        MainView m_subjectView;

        public HelpModule(IUnityContainer container, IRegionManager regionManager)
        {
            m_unityContainer = container;
            m_regionManager = regionManager;
        }

        public void Initialize()
        {
            AddHelpView();
        }

        private void AddHelpView()
        {
            HelpView view = m_unityContainer.Resolve<HelpView>();
           // TestClass test = m_unityContainer.Resolve<TestClass>();
            IRegion reg = m_regionManager.Regions["HelpRegion"];
            Panel panel = reg.RegionHost as Panel;
            reg.Add(view, "HelpView");
            reg.Activate(view);
            view.Dock = DockStyle.Top;
            panel.Height = view.Height;
          //  test.RunConsole();
        }

    }
}
