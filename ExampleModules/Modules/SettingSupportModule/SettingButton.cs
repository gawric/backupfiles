﻿using DataModel.configModel;
using EventManager;
using ExampleModules.Controller.Config;
using Microsoft.Practices.Unity;
using Prism.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExampleModules.Modules.SettingSupportModule
{
    public class SettingButton
    {
        IEventAggregator m_eventAggregator;
        IUnityContainer container;
        ConfigController cfg;

        public SettingButton(IUnityContainer container, IEventAggregator m_eventAggregator)
        {
            this.m_eventAggregator = m_eventAggregator;
            cfg = container.Resolve<ConfigController>();
            Subscribe();
        }

        //подписка для MainView
        public void Subscribe()
        {
           // m_eventAggregator.GetEvent<SettingButtonCLickEvent>().Subscribe(SettingClickButton);
        }

        public void SettingClickButton(string obj)
        {
            

        }
    }
}
