﻿using EventManager;
using ExampleModules.Views;
using Microsoft.Practices.Unity;
using Prism.Events;

namespace ExampleModules.Modules.SettingSupportModule
{
    public class SettingUpdateElements
    {
        private IEventAggregator m_eventAggregator;
        private IUnityContainer container;
        private string[] Info = new string[2];

        public SettingUpdateElements(IUnityContainer container , IEventAggregator m_eventAggregator)
        {
            this.m_eventAggregator = m_eventAggregator;

        }

        public void UpdateSettingElemetns(string elemetns , string text)
        {
            Info[0] = elemetns;
            Info[1] = text;

            m_eventAggregator.GetEvent<SettingUpdateViewElemetns>().Publish(Info);
        }

       
       
    }
}
