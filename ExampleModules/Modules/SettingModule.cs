﻿using ExampleModules.Views;
using Microsoft.Practices.Unity;
using Prism.Modularity;
using Prism.Regions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ExampleModules
{
    public class SettingModule : IModule
    {
        IUnityContainer m_unityContainer;
        IRegionManager m_regionManager;
        SettingView m_subjectView;

        public SettingModule(IUnityContainer container, IRegionManager regionManager)
        {
            m_unityContainer = container;
            m_regionManager = regionManager;
        }

        public void Initialize()
        {
            AddSettingView();
        }

        private void AddSettingView()
        {
            SettingView view = m_unityContainer.Resolve<SettingView>();
            IRegion reg = m_regionManager.Regions["SettingRegion"];
            Panel panel = reg.RegionHost as Panel;
            reg.Add(view, "SettingView");
            reg.Activate(view);
            view.Dock = DockStyle.Top;
            panel.Height = view.Height;
        }
    }
}
