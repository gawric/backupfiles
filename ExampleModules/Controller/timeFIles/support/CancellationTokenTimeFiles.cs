﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ExampleModules.Controller.timeFIles.support
{
    public class CancellationTokenTimeFiles
    {
        CancellationTokenSource tokenSource;
        CancellationToken token;

        public CancellationToken TokenCancelGenerated()
        {
            tokenSource = new CancellationTokenSource();
            token = tokenSource.Token;
            return token;
        }

        public void setStop()
        {
            if(tokenSource != null)
            {
                tokenSource.Cancel();
            }
            
        }
    }
}
