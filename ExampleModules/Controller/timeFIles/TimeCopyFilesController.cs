﻿using EventManager;
using ExampleModules.Controller.CopyFiles;
using ExampleModules.Controller.sqlite;
using ExampleModules.Controller.timeFIles.support;
using Microsoft.Practices.Unity;
using Prism.Events;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ExampleModules.Controller.timeFIles
{
    public class TimeCopyFilesController
    {
  
       private IEventAggregator m_eventAggregator;
       private CopyFilesRemotePath CopyObj;
       private SqliteController sqlite;
       private CancellationTokenTimeFiles cansel;

        public TimeCopyFilesController(IUnityContainer cont, IEventAggregator m_eventAggregator)
        {
            this.m_eventAggregator = m_eventAggregator;
            CopyObj = cont.Resolve<CopyFilesRemotePath>();
            sqlite = cont.Resolve<SqliteController>();
        }

        private void runCopy(string inDir , string outDir)
        {
            string filename = GetRootName(inDir);
            DateTime curDate = DateTime.Now;

            CopyObj.CopyFilesRemote(filename , curDate.ToString(), inDir , outDir);
         }
        //date - дата из конфига во сколько часов делпать beckup
        public async void start(string date , string inDir , string outDir)
        {
            cansel = new CancellationTokenTimeFiles();
            CancellationToken token = cansel.TokenCancelGenerated();

            try
            {
                //запуск
                await Task.Run(() => AsyncTimeRun(date, inDir, outDir, token));
            }
            catch(OperationCanceledException e)
            {
                Console.WriteLine(e.ToString());
            }
         
        }

        private void AsyncTimeRun(string date, string inDir, string outDir , CancellationToken canselationToken)
        {
            DateTime date_config = DateTime.Parse(date);

            while(true)
            {
                Thread.Sleep(4000);

                Console.WriteLine("scanning sync files");

                  //Грохает поток когда меняется отслеживаемый файл
                    if (canselationToken.IsCancellationRequested)
                    {
                        canselationToken.ThrowIfCancellationRequested();
                    }
               
                asyncBeginScanner(date_config, inDir, outDir);
            }
            
        }
        //постоянно сканирует время и когда подходит нужный момент делает сохранение файлов
       private void asyncBeginScanner(DateTime date_config, string inDir, string outDir)
       {
                TimeSpan TodayTime = DateTime.Now.TimeOfDay;
                TimeSpan s = date_config.TimeOfDay;
                TimeSpan n = TodayTime;

            int result = TimeSpan.Compare(s, n);

            if (result >= 1)
            {
                Console.WriteLine("TimeCopyFilesController Время для Синхронизации не пришло");
            }
            else
            {
                CheckTimeStart(inDir , outDir);
            }

            
        }

        private void CheckTimeStart(string inDir , string outDir)
        {

            //текущая дата
            DateTime dt1 = DateTime.Now.Date;

            //дата последней синхронизации
            DateTime dt2 = LastSyncDateTime();
           
            //если время пришло для синхронизации и ее не было проведено 
            //мы пытаемся синхронизироваться
            if (dt1 == dt2)
            {
                // Console.WriteLine("Последняя синхронизация была произведена "+dt2 );
            }
            else
            {
                //  Console.WriteLine("Синхронизация не была произведена " + dt2);

                runCopy(inDir, outDir);
            }
        }
        
        private DateTime LastSyncDateTime()
        {
            SQLiteDataReader reader = sqlite.sqlSelectLastRow();
          

            string dateTime = "";

            while (reader.Read())
            {
                dateTime = reader["dateTime"].ToString();
            }

           
            
           
            if (dateTime.Equals(""))
            {
                string text = "xx.xx.xxxx 00:00:00";
                UpdateLabelLastSave(text);
                //если в базе нет файлов по синхр.
                return Convert.ToDateTime("00:00:00").Date.AddDays(-1);
            }
            else
            {
               // string text = Convert.ToDateTime(dateTime).Date.ToString();
                UpdateLabelLastSave(dateTime);
                //дата из базы
                return Convert.ToDateTime(dateTime).Date;
            }

            
        }


        public void setStop()
        {
            if(cansel != null)
            {
                cansel.setStop();
            }
            else
            {
                Console.WriteLine("Поток SyncFile не запущен т.к TokenCalsellation пустой");
            }
            
        }



        //begin начало имени файла
        //режет автоматом от конца до символы вхождения
        private string GetRootName(string str)
        {
            int begin = str.LastIndexOf("\\") + 1;

            return str.Substring(begin);

        }

        private void UpdateLabelLastSave(string text)
        {
            string[] arr = new string[2];
            arr[0] = "visibleLastSaveLabel";
            arr[1] = text;

            m_eventAggregator.GetEvent<MainUpdateViewElemetns>().Publish(arr);
        }
    }
}

