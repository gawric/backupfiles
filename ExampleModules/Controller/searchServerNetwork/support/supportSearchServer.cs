﻿using DataModel.SocketModel;
using EventManager;
using ExampleModules.Controller.socketClient.client;
using Microsoft.Practices.Unity;
using Prism.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ExampleModules.Controller.searchServerNetwork.support
{
    public class supportSearchServer
    {
        private AsyncClientSocket AsyClient;

       
        public bool endRunning = false;
        private IEventAggregator m_eventAggregator;

        public supportSearchServer(IUnityContainer container , IEventAggregator m_eventAggregator)
        {

             AsyClient = container.Resolve<AsyncClientSocket>();
             this.m_eventAggregator = m_eventAggregator;

        }


        public async void start(string ip, int port, IProgress<SocketClientModel> progress, CancellationToken token)
        {
            //список адресов  
            //из него сканируем всю его подсеть
            string[] arr = generatedScanIpAdresss(ip);

            // Console.WriteLine("Check " + checkRunningTask);
            try
            {

                await Task.Run(() => BeginAsync(arr, port, progress, token));
                //  checkRunningTask = true;
            }
            catch (OperationCanceledException e)
            {
                Console.WriteLine(e);
            }
            finally
            {
                // tokenSource.Dispose();

                //checkRunningTask = false;
            }




        }


        public void BeginAsync(string[] arr, int port, IProgress<SocketClientModel> progress, CancellationToken cancellationToken)
        {

            startAsync(arr, port, progress, cancellationToken);

        }

        private string[] generatedScanIpAdresss(string ipAddress)
        {
            string[] ipRange = new string[254];
            int count = ipRange.Length;

            for (int f = 0; f < count; f++)
            {
                int end = ipAddress.LastIndexOf(".") + 1;
                int begin = 0;
                int f2 = f + 1;
                ipRange[f] = ipAddress.Substring(begin, end) + f2;
            }

            return ipRange;

        }


        private async void scanningNetwork(string ip, int port, IProgress<SocketClientModel> progress)
        {
            await Task.Run(() =>
            {
                Socket client = null;
                try
                {
                    // Console.WriteLine("Запуск асинк");
                    client = AsyClient.ClientStart(ip, port);
                }
                catch (System.Net.Sockets.SocketException e)
                {
                    Console.WriteLine(e.ToString());
                }




                return client;
            }
           );
        }

        private void startAsync(string[] arr, int port, IProgress<SocketClientModel> progress, CancellationToken cancellationToken)
        {
            int count = arr.Length;

            SocketClientModel model = new SocketClientModel();
            model.setStatus("Запуск....");
            progress.Report(model);


            for (int d = 0; d < count; d++)
            {
                //сканирует сеть в поиске ip сервера
                //если удается подключится передает в progress клиента
                scanningNetwork(arr[d], port, progress);
           

                if (cancellationToken.IsCancellationRequested)
                {
                    model.setStatus("STOP");
                    progress.Report(model);

                    cancellationToken.ThrowIfCancellationRequested();
                }

                Thread.Sleep(10500);

                string s = "ip:" + arr[d];

                model.setStatus(s);
                progress.Report(model);

            }
        }

        public Progress<SocketClientModel> SearchNetworkCallback()
        {
           // updSettingLabel.updateSettingSearchNetworkTextStart("Подготовка");


            string[] updateSettingView = new string[2];

            updateSettingView[0] = "settingUpdateSearchNetworkText";
            updateSettingView[1] = "Подготовка";

            string[] MainSettingView = new string[2];

           

            m_eventAggregator.GetEvent<SettingUpdateViewElemetns>().Publish(updateSettingView);

            return new Progress<SocketClientModel>(update => {

                string statusClient = update.getStatus();

                //здесь обрабатываем событие если мы получили клиента
                //дальше код не реализован
                if (statusClient.Equals("OK"))
                {
                    // updSettingLabel.updateSettingSearchNetworkTextOK("Завершено");
                    //updMainLabel.updateStatusConnectLabelOK("Подключено");
                    updateSettingView[0] = "settingUpdateSearchNetworkText";
                    updateSettingView[1] = "Завершенно";

                    MainSettingView[0] = "mainUpdateStatusConnServer";
                    MainSettingView[1] = "Подключено";

                    m_eventAggregator.GetEvent<SettingUpdateViewElemetns>().Publish(updateSettingView);
                    m_eventAggregator.GetEvent<MainUpdateViewElemetns>().Publish(MainSettingView);

                    endRunning = false;
                }
                else if (statusClient.Equals("STOP"))
                {
                    // updSettingLabel.updateSettingSearchNetworkTextOK("Отмена");
                    // updMainLabel.updateStatusConnectLabelError("Нет подключения");

                    updateSettingView[0] = "settingUpdateSearchNetworkText";
                    updateSettingView[1] = "Отмена";

                    m_eventAggregator.GetEvent<SettingUpdateViewElemetns>().Publish(updateSettingView);

                    MainSettingView[0] = "mainUpdateStatusConnServer";
                    MainSettingView[1] = "Не Подключено";

                 //   m_eventAggregator.GetEvent<SettingUpdateViewElemetns>().Publish(updateSettingView);
                    m_eventAggregator.GetEvent<MainUpdateViewElemetns>().Publish(MainSettingView);

                    endRunning = false;
                }
                else
                {
                    updateSettingView[0] = "settingUpdateSearchNetworkText";
                    updateSettingView[1] = statusClient;

                    m_eventAggregator.GetEvent<SettingUpdateViewElemetns>().Publish(updateSettingView);
                    // updSettingLabel.updateSettingSearchNetworkTextSearch(statusClient);
                }

            });
        }

        public string GetLocalIPAddress()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            string status = "ERROR";
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }


            return status;
        }

        public bool getEndRunning()
        {
            return endRunning;
        }

        public void setEndRunning(bool bl)
        {
            endRunning = bl;
        }


    }
}
