﻿using ExampleModules.Controller.searchServerNetwork.support;
using ExampleModules.Modules.MainSupportModule;
using Microsoft.Practices.Unity;
using System;
using System.Threading;

namespace ExampleModules.Controller.searchServerNetwork
{
    public class searchServer
    {
        private supportSearchServer supportSearch;
        private CancellationTokenSource tokenSource;
        private CancellationToken token;
        private MainPushNotifi notifyIcon;

        public searchServer(IUnityContainer container)
        {
            supportSearch = container.Resolve<supportSearchServer>();
            notifyIcon = container.Resolve<MainPushNotifi>();
        
        }

        public void Scan()
        {

            string ipNetwork = supportSearch.GetLocalIPAddress();

            if (ipNetwork.Equals("ERROR"))
            {

            }
            else
            {
                //запущен поток сканирования или нет
                bool Running = supportSearch.endRunning;
                //bool Running2 = supportSearch.MyParam;
               // Console.WriteLine(supportSearch.ToString());
                Running = supportSearch.getEndRunning();
                if (Running == false)
                {

                    supportSearch.endRunning = true;
                   

                    //обновляет ход статуса поиска
                    var progress = supportSearch.SearchNetworkCallback();

                    //для отмены потока
                    TokenCancelGenerated();

                    //старт поиска
                    supportSearch.start(ipNetwork, 2026, progress, token);
                }



            }

        }

        private CancellationToken TokenCancelGenerated()
        {

            tokenSource = new CancellationTokenSource();
            token = tokenSource.Token;
            return token;
        }

        public void setStop(CancellationTokenSource tokenSource)
        {
            this.tokenSource = tokenSource;
        }
        public void getStop()
        {
            if(tokenSource != null)
            {
                tokenSource.Cancel();
            }
             
        }

    }
}
