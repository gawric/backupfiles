﻿using EventManager;
using ExampleModules.Controller.Config;
using ExampleModules.Modules.MainSupportModule;
using Microsoft.Practices.Unity;
using Prism.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExampleModules.Controller.CopyFiles
{
    public class CopyFilesRemotePath
    {
        private MainPushNotifi NotifyController;

        IEventAggregator m_eventAggregator;

        public CopyFilesRemotePath(IUnityContainer cont , IEventAggregator m_eventAggregator)
        {
           NotifyController = cont.Resolve<MainPushNotifi>();
           this.m_eventAggregator = m_eventAggregator;
        }

        public void CopyFilesRemote(string filename, string date, string dirRootFiles , string outDir)
        {
           
            string[] updateMainView = new string[2];
            updateMainView[0] = "visibleStatusLabel";
            updateMainView[1] = "Обновление";

            m_eventAggregator.GetEvent<MainUpdateViewElemetns>().Publish(updateMainView);

            string time = "Время: " + DateTime.Now.ToString("HH:mm:ss");
            NotifyPush(filename, time);


            //откуда брать файл
            string inDirFiles = dirRootFiles;

            //куда его сохранять
            string outDirFiles = outDir;


            //реализация копирования
            AsyncCopyFiles(inDirFiles, outDirFiles);


            updateMainView[0] = "visibleStatusLabel";
            updateMainView[1] = "Работает";

            m_eventAggregator.GetEvent<MainUpdateViewElemetns>().Publish(updateMainView);

           // mainLabel.updateStatusVisibleStatusLabelOK("Работает");

        }

        private async void AsyncCopyFiles(string inDirFiles, string outDirFiles)
        {
            IProgress<string[]> progress = createCallBack();

            WorkerCopyFiles copyFunction = createObjectWorkerCopy();

            await Task.Run(() => copyFunction.AsyncCopyFiles(progress, inDirFiles, outDirFiles));
        }


        private WorkerCopyFiles createObjectWorkerCopy()
        {
            return new WorkerCopyFiles();
        }

        private IProgress<string[]> createCallBack()
        {
            CopyFilesCallBack copyObjectCallBack = new CopyFilesCallBack(m_eventAggregator);

            return copyObjectCallBack.CopyCallBack();
        }
        private void NotifyPush(string filename, string date)
        {
            NotifyController.pushNotification(filename, date);
        }

    }
}
