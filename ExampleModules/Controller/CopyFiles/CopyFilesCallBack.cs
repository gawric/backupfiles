﻿using EventManager;
using Microsoft.Practices.Unity;
using Prism.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExampleModules.Controller.CopyFiles
{
   public class CopyFilesCallBack
   {
   
        IEventAggregator m_eventAggregator;

        public CopyFilesCallBack(IEventAggregator m_eventAggregator)
        {
            this.m_eventAggregator = m_eventAggregator;
        }
        //если копирование оказалось успешным мы возвразаем CallBack для добавление в базы
        public Progress<string[]> CopyCallBack()
        {
            return new Progress<string[]>(update =>
            {


                string date = DateTime.Now.ToString();

                string dirIn = update[0];
                string status = update[1];
                string lenght = update[2];
              

                string[] updateMainView = new string[2];
                string[] updateMainListBox = new string[5];

                if (update[1].Equals("OK"))
                {
                    

                    updateMainView[0] = "visibleLastSaveLabel";
                    updateMainView[1] = date.ToString();

                    //[0] - dir или FileName
                    //[1] - lenght
                    //[2] - dateTime
                    //[3] - nameFiles
                    updateMainListBox[0] = dirIn;
                    updateMainListBox[1] = lenght;
                    updateMainListBox[2] = date.ToString();
                    updateMainListBox[3] = GetRootName(dirIn);

                    m_eventAggregator.GetEvent<MainUpdateViewElemetns>().Publish(updateMainView);
                    m_eventAggregator.GetEvent<MainListBoxChengeItems>().Publish(updateMainListBox);


                    Console.WriteLine("Копирования завершенно: " + update[1]);
                }
                else
                {
                   

                    updateMainView[0] = "visibleLastSaveLabel";
                    updateMainView[1] = date.ToString();

                    updateMainListBox[0] = "error: " + dirIn + " " + date.ToString();
                    updateMainListBox[1] = status;



                    m_eventAggregator.GetEvent<MainUpdateViewElemetns>().Publish(updateMainView);
                    m_eventAggregator.GetEvent<MainListBoxChengeItems>().Publish(updateMainListBox);

                    Console.WriteLine("Копирования завершенно: " + update[1]);
                }

            });
        }

        private string GetRootName(string str)
        {
            int begin = str.LastIndexOf("\\") + 1;

            return str.Substring(begin);

        }
    }

}
