﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExampleModules.Controller.CopyFiles
{
    class WorkerCopyFiles
    {


        public void AsyncCopyFiles(IProgress<string[]> progress, string inDirFiles, string outDirFIles)
        {
            //0 - путь или имя файла
            //1 - статус файла
            string[] status = new string[3];

            try
            {
                if (File.Exists(outDirFIles))
                {
                    // Will not overwrite if the destination file already exists.
                    File.Copy(inDirFiles, outDirFIles, true);

                    status[0] = inDirFiles;
                    status[1] = "OK";
                    //возвращает размер файла в байтах
                    status[2] = getSizeFiles(inDirFiles);
                    progress.Report(status);
                }
                else
                {
                    using (FileStream fs = File.Create(outDirFIles))
                    {

                    }

                    File.Copy(inDirFiles, outDirFIles, true);


                    status[0] = inDirFiles;
                    status[1] = "OK";

                    progress.Report(status);
                }

            }

            // Catch exception if the file was already copied.
            catch (IOException copyError)
            {
                Console.WriteLine(copyError.Message);

                status[0] = inDirFiles;
                status[1] = "ERROR: " + "Ошибка копирования файла";
                progress.Report(status);
            }
        }

        private String getSizeFiles(string dirIn)
        {
            System.IO.FileInfo file = new System.IO.FileInfo(dirIn);
           return  file.Length.ToString();
        }
    }
}
