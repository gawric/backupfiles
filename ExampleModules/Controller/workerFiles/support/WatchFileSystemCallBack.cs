﻿using DataModel.watchModel;
using ExampleModules.Controller.CopyFiles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace ExampleModules.Controller.workerFiles.support
{
    class WatchFileSystemCallBack
    {

        public Progress<WatchFileSystemCallBackModel> WatchCallBack(CopyFilesRemotePath CopyFiles)
        {
            return new Progress<WatchFileSystemCallBackModel>(update =>
            {
                CopyFiles.CopyFilesRemote(update.filename, update.date, update.dirRootFiles , update.dirOutFiles);
            });
        }
    }
}
