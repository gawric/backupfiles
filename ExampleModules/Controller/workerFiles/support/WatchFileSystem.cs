﻿using DataModel.statVariable;
using DataModel.watchModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ExampleModules.Controller.workerFiles.support
{

    class WatchFileSystem
    {
       

        public void startFileWatch(string watchFileName , string dirOutFiles , IProgress<WatchFileSystemCallBackModel> progress, CancellationToken cancellationToken)
        {
            string rootFolder = GetRootFolder(watchFileName);
            string rootName = GetRootName(watchFileName);

            variable.inRootFolder = rootFolder;
            variable.inFileName = rootName;

            FileSystemWatcher watcher = new FileSystemWatcher(rootFolder, rootName);

            // watcher.Path =rootFolder;
            watcher.InternalBufferSize = 4096;

            // Begin watching.
            watcher.EnableRaisingEvents = true;

            EventChangeFile(progress, watcher, cancellationToken, watchFileName , dirOutFiles);
        }



        public void EventChangeFile(IProgress<WatchFileSystemCallBackModel> progress, FileSystemWatcher watcher, CancellationToken cancellationToken, string rootFiles , string dirOutFiles)
        {
            // only rename event
            while (true)
            {
                WaitForChangedResult result = watcher.WaitForChanged(WatcherChangeTypes.Changed | WatcherChangeTypes.Renamed, 1000);

                //Грохает поток когда меняется отслеживаемый файл
                if (cancellationToken.IsCancellationRequested)
                {

                    //  progress.Report(model);
                    cancellationToken.ThrowIfCancellationRequested();
                }

                switch (result.ChangeType)
                {
                    case WatcherChangeTypes.Changed:
                        WatchFileSystemCallBackModel model = createModel(result.Name, DateTime.Now.ToString(), rootFiles , dirOutFiles);
                        progress.Report(model);
                        break;

                        //case WatcherChangeTypes.Renamed:
                        //   Console.WriteLine("{0}: File '{1}' was renamed to '{2}'", DateTime.Now, result.OldName, result.Name);
                        //  break;
                }
            }
        }

        private WatchFileSystemCallBackModel createModel(String filename, String DateTime, string rootFiles , string dirOutFiles)
        {
            WatchFileSystemCallBackModel model = new WatchFileSystemCallBackModel();
            model.filename = filename;
            model.date = DateTime;
            model.dirRootFiles = rootFiles;
            model.dirOutFiles = dirOutFiles;
            return model;
        }



        private string GetRootFolder(string str)
        {
            int end = str.LastIndexOf("\\") + 1;
            int begin = 0;

            return str.Substring(begin, end);

        }

        //begin начало имени файла
        //режет автоматом от конца до символы вхождения
        private string GetRootName(string str)
        {
            int begin = str.LastIndexOf("\\") + 1;

            return str.Substring(begin);

        }

    }
}
