﻿using EventManager;
using ExampleModules.Controller.CopyFiles;
using ExampleModules.Controller.workerFiles.support;
using Microsoft.Practices.Unity;
using Prism.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ExampleModules.Controller.workerFiles
{
   public class WorkerFilesController
    {
        private WatchFileSystem watch;
        private CancellationTokenWatchFiles tokenWatchservice;
        private CopyFilesRemotePath CopyFiles;
        private IEventAggregator m_eventAggregator;

        public WorkerFilesController(IUnityContainer cont, IEventAggregator m_eventAggregator)
        {
            watch = new WatchFileSystem();
            CopyFiles = cont.Resolve<CopyFilesRemotePath>();
            this.m_eventAggregator = m_eventAggregator;
        }
        //подготовка пааметров к сканированию папки
        public async void WatchFile(string filename , string dirOutFiles)
        {
            
            //callback object
            WatchFileSystemCallBack callBack = new WatchFileSystemCallBack();

            //token genreted - убить поток если сменится папка
            tokenWatchservice = new CancellationTokenWatchFiles();


            var progress = callBack.WatchCallBack(CopyFiles);
            CancellationToken token = tokenWatchservice.TokenCancelGenerated();

            try
            {
                // statusMain.updateStatusVisibleStatusLabelOK("Работает");
                string[] mainSettingView = new string[2];

                mainSettingView[0] = "visibleStatusLabel";
                mainSettingView[1] = "Работает";

                m_eventAggregator.GetEvent<MainUpdateViewElemetns>().Publish(mainSettingView);

                //запуск
                await Task.Run(() => watch.startFileWatch(filename, dirOutFiles ,  progress, token));

            }
            catch (OperationCanceledException e)
            {
                Console.WriteLine(e);
            }


        }

        //пересоздание нового слушателя папки
        public void ChangeWatchFile(string filename , string dirOutFiles)
        {

            if (tokenWatchservice != null)
            {
                //убить поток 
                tokenWatchservice.setStop();
            }

            string[] mainSettingView = new string[2];

            mainSettingView[0] = "visibleStatusLabel";
            mainSettingView[1] = "Перезапуск..";

            m_eventAggregator.GetEvent<MainUpdateViewElemetns>().Publish(mainSettingView);

            //новый поток
            WatchFile(filename , dirOutFiles);
        }


        //Останавливает поток синхронизации
        public void StopWatchFile()
        {
            if (tokenWatchservice != null)
            {
                //убить поток 
                tokenWatchservice.setStop();
            }

            string[] mainSettingView = new string[2];

            mainSettingView[0] = "visibleStatusLabel";
            mainSettingView[1] = "Остановлен";

            m_eventAggregator.GetEvent<MainUpdateViewElemetns>().Publish(mainSettingView);
        }

        





    }
}
