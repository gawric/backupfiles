﻿using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ExampleModules.Controller.socketClient.client
{
    public class AsyncClientSocket
    {

        // ManualResetEvent instances signal completion.  
        private ManualResetEvent connectDone;
        private ManualResetEvent sendDone;
        private ManualResetEvent receiveDone;

        private ClientConnectCallback clientConnCallBack;
        private ClientReceiveCallback clientConnReceiveCallBack;
        private ClientSendCallback clientConnSendCallBack;


        public AsyncClientSocket(IUnityContainer container)
        {

        }

        public Socket ClientStart(string strIpAddress, int port)
        {
            CreateObjectNetworkAsync();
            Socket client = null;
            try
            {
                IPAddress address = IPAddress.Parse(strIpAddress);

                IPEndPoint remoteEP = new IPEndPoint(address, port);

                // Create a TCP/IP socket.  
                client = new Socket(address.AddressFamily,
                   SocketType.Stream, ProtocolType.Tcp);

                // Connect to the remote endpoint.  
                client.BeginConnect(remoteEP,
                    new AsyncCallback(ConnectCallback), client);
                connectDone.WaitOne();

                //client.Connected

                // Send test data to the remote device.  
                clientConnSendCallBack.Send(client, "This is a test<EOF>");
                sendDone.WaitOne();

                // Receive the response from the remote device.  
                clientConnReceiveCallBack.Receive(client);
                receiveDone.WaitOne();

                // Write the response to the console.  
                Console.WriteLine("Response received : {0}", clientConnReceiveCallBack.getResponce());

                // Release the socket.  
                client.Shutdown(SocketShutdown.Both);
                client.Close();

                return client;
            }
            catch (System.Net.Sockets.SocketException e)
            {
                Console.WriteLine("2");
                //  Console.WriteLine(e);
                return null;
            }
            finally
            {
                client.Close();

            }


        }

        public void ConnectCallback(IAsyncResult ar)
        {
            Socket client = null;
            try
            {

                client = (Socket)ar.AsyncState;
                client.EndConnect(ar);
                Console.WriteLine("Socket connected to {0}",
                    client.RemoteEndPoint.ToString());


                connectDone.Set();
            }
            catch (Exception e)
            {

                Console.WriteLine(e.ToString());
                client.Close();
            }

        }



        public void CreateObjectNetworkAsync()
        {
            connectDone = new ManualResetEvent(false);
            sendDone = new ManualResetEvent(false);
            receiveDone = new ManualResetEvent(false);

            clientConnCallBack = new ClientConnectCallback(connectDone);
            clientConnReceiveCallBack = new ClientReceiveCallback(receiveDone);
            clientConnSendCallBack = new ClientSendCallback(sendDone);
        }


    }




    // State object for receiving data from remote device.  
    public class StateObject
    {
        // Client socket.  
        public Socket workSocket = null;
        // Size of receive buffer.  
        public const int BufferSize = 256;
        // Receive buffer.  
        public byte[] buffer = new byte[BufferSize];
        // Received data string.  
        public StringBuilder sb = new StringBuilder();
    }
}
