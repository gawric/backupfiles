﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ExampleModules.Controller.socketClient.client
{
    class ClientConnectCallback
    {
        public ManualResetEvent connectDone;


        public ClientConnectCallback(ManualResetEvent connectDone)
        {
            this.connectDone = connectDone;
        }

        public void ConnectCallback(IAsyncResult ar)
        {
            try
            {
                // Retrieve the socket from the state object.  
                Socket client = (Socket)ar.AsyncState;

                // Complete the connection.  
                client.EndConnect(ar);

                Console.WriteLine("Socket connected to {0}",
                    client.RemoteEndPoint.ToString());

                // Signal that the connection has been made.  
                connectDone.Set();

            }
            catch (SocketException e)
            {

                Console.WriteLine(e.ToString());
            }
        }


    }
}
