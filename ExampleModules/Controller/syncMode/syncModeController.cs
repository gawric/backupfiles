﻿using DataModel.statVariable;
using ExampleModules.Controller.sqlite;
using ExampleModules.Controller.timeFIles;
using ExampleModules.Controller.workerFiles;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExampleModules.Controller.syncMode
{
   public class syncModeController
    {
        private WorkerFilesController worker;
        private TimeCopyFilesController time_copy;
        private SqliteController sqlite;

        public syncModeController(IUnityContainer cont)
        {
            //WatchFileSystem
            worker = cont.Resolve<WorkerFilesController>();
            sqlite = cont.Resolve<SqliteController>();
            time_copy = cont.Resolve<TimeCopyFilesController>();
        }

        public void selectSync(string select_sync, string inDirFiles , string dirOutFiles , string time_sync)
        {

            if (select_sync.Equals(variable.runtime_sync))
            {
                //останавливаем TimeSync
                TimeSyncStop();

                ChangeWatchFile(inDirFiles , dirOutFiles);
            }
            else
            {
                //останавливаем TimeSync если он был запущен
                TimeSyncStop();

                //останавливаем runtime поток если он есть 
                runtimeSyncStop();

                //запускаем sync time поток
                timeSync(time_sync , inDirFiles , dirOutFiles);
            }


        }


        //запуск синхронизации по изменению файла
        private void runtimeSync(string inDirFiles , string dirOutFiles)
        {
            worker.WatchFile(inDirFiles , dirOutFiles);
        }


        private void runtimeSyncStop()
        {
            worker.StopWatchFile();
        }
        private void TimeSyncStop()
        {
            time_copy.setStop();
        }

        public void ChangeWatchFile(string filename , string dirOutFiles)
        {
            //Console.WriteLine("ChangeWatchFile Runtime");
            worker.ChangeWatchFile(filename , dirOutFiles);
        }

        //===================

        public void ChangeTimeSync()
        {
            Console.WriteLine("Запускаем тайм синк");
        }

        //запуск синхронизации по времени
        public void timeSync(string date , string inDir , string outDir)
        {

            time_copy.start(date, inDir, outDir);
        }
        //если по каким-то причинам конфиг не загружен 
        //или не получены данные для запуска
        public void noStart()
        {
            Console.WriteLine("ERROR: Конфигурация для работы клиента не загружена!!!!!!>>>>>");
        }
    }

}
