﻿using DataModel.statVariable;
using ExampleModules.Controller.sqlite.support;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ExampleModules.Controller.sqlite
{
    public class SqliteController
    {
        private CreateBaseSqlite  create;
        private DeleteBaseSqlite  delete;
        private InsertBaseSqllite insert;
        private SelectBaseSqllite select;

        private SQLiteConnection connect;

        public SqliteController()
        {
            create = new CreateBaseSqlite();
            insert = new InsertBaseSqllite();
            select = new SelectBaseSqllite();
            delete = new DeleteBaseSqlite();

        }

        public void InsertRows(string dir, long length, string dateTime, string nameFiles)
        {
            insert.InsertConnect(getConnect());
            insert.InsertRows(dir, length, dateTime, nameFiles);
        }

        private string getDirBases()
        {
            string rootPath = variable.root_folder_programm;
            string basesName = variable.sqlite_bases_name;
            return  rootPath + "\\" + basesName;
        }

        public async void CreateBaseSqlite()
        {
            //проверяет есть ли файл  и создает сам его
            string check = await Task.Run(() => create.checkCreateBases());

            if (check.Equals("OK"))
            {
                //если с базой все ок, мы не говорим пользователю 
                // MessageBox.Show("База данных ОК");
            }
            else
            {
                MessageBox.Show(check);
                Environment.Exit(0);
            }

        }

        public void startConnect()
        {
            string filename = getDirBases();
            connect = new SQLiteConnection("Data Source=" + filename + ";Version=3;");
            connect.Open();
        }

        public SQLiteConnection getConnect()
        {
            if(connect == null)
            {
                startConnect();
            }
            else
            {

            }
           return  connect;
        }

        public SQLiteDataReader sqlSelect()
        {
            
            return select.SqliteSelect(getConnect());
        }

        public SQLiteDataReader sqlSelectLastRow()
        {
            return select.SqliteselectLastRows(getConnect());
        }
        //очищает всю таблицу AsyncFiles
        public void ClearAsyncFilesTable()
        {
            delete.ClearFilesAsyncTable(getConnect());
        }
    }
}
