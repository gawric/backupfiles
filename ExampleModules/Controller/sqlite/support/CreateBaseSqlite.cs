﻿using DataModel.statVariable;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExampleModules.Controller.sqlite.support
{
    class CreateBaseSqlite
    {
        private SQLiteConnection m_dbConn;


        public CreateBaseSqlite()
        {

        }

        public string checkCreateBases()
        {
            string check;
            string rootPath = variable.root_folder_programm;
            string basesName = variable.sqlite_bases_name;
            string path = rootPath + "\\" + basesName;

            try
            {

                //true - файл есть
                //false файла нет и нет разрешений
                if (File.Exists(path))
                {
                    check = "OK";
                }
                else
                {
                    SQLiteConnection.CreateFile(path);
                    check = CreateTableBases(path);
                }


            }
            catch (Exception ex)
            {
                check = ex.ToString();
                Console.WriteLine(ex.ToString());
            }

            return check;
        }

        public string CreateTableBases(string dbFileName)
        {
            string status;
            try
            {
                m_dbConn = new SQLiteConnection("Data Source=" + dbFileName + ";Version=3;");
                m_dbConn.Open();
                SQLiteCommand m_sqlCmd = new SQLiteCommand();
                m_sqlCmd.Connection = m_dbConn;


                m_sqlCmd.CommandText = "CREATE TABLE IF NOT EXISTS FilesAsync (id INTEGER PRIMARY KEY AUTOINCREMENT, dir TEXT, length INTEGER , dateTime STRING , nameFiles String)";
                m_sqlCmd.ExecuteNonQuery();

                SQLiteCommand m_sqlCmd2 = new SQLiteCommand();
                m_sqlCmd2.Connection = m_dbConn;


                m_sqlCmd2.CommandText = "CREATE TABLE IF NOT EXISTS InfoAsync (id INTEGER PRIMARY KEY AUTOINCREMENT, dir TEXT, dateTimeLastSync STRING , nameFiles String)";
                m_sqlCmd2.ExecuteNonQuery();

                status = "OK";
            }
            catch (SQLiteException ex)
            {
                status = "ERROR: проблемы с созданием базы данных";
                Console.WriteLine(ex.ToString());

            }
            return status;
        }

    }
}
