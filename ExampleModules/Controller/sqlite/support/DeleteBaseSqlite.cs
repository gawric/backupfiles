﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ExampleModules.Controller.sqlite.support
{
    class DeleteBaseSqlite
    {

        public void ClearFilesAsyncTable(SQLiteConnection connect)
        {
            if (connect.State != ConnectionState.Open)
            {
                MessageBox.Show("Open connection with database");
        
            }

            try
            {

                using (SQLiteCommand fmd1 = connect.CreateCommand())
                {

                    fmd1.CommandText = @"DELETE FROM FilesAsync";
                    fmd1.ExecuteNonQuery();
                }

                Console.WriteLine("*********Clear FilesAsync Table LITE**********");

            }
            catch (SQLiteException ex)
            {
                MessageBox.Show("Error: " + ex.Message);
  
            }
        }
    }
}
