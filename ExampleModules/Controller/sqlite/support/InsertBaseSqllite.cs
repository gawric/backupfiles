﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ExampleModules.Controller.sqlite.support
{
    class InsertBaseSqllite
    {
        private SQLiteConnection connect;

        public InsertBaseSqllite()
        {

        }

        public void InsertConnect(SQLiteConnection connect)
        {
            this.connect = connect;
        }

        public void InsertRows(string dir , long length , string dateTime , string nameFiles)
        {
            if (connect.State != ConnectionState.Open)
            {
                MessageBox.Show("Open connection with database");
                return;
            }
            try
            {
                using (SQLiteCommand fmd1 = connect.CreateCommand())
                {

                    fmd1.CommandText = "INSERT INTO FilesAsync ('dir', 'length' , 'dateTime', 'nameFiles') values ('" + dir + "', '" + length + "' , '" + dateTime + "', '" + nameFiles + "')";
                    fmd1.ExecuteNonQuery();
                }

                Console.WriteLine("*********Insert Line SQL LITE**********");
            }
            catch (SQLiteException ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
        }
    }
}
