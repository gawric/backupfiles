﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ExampleModules.Controller.sqlite.support
{
    class SelectBaseSqllite
    {

        public SQLiteDataReader SqliteSelect(SQLiteConnection connect)
        {
            String sqlQuery;

            if (connect.State != ConnectionState.Open)
            {
                MessageBox.Show("Open connection with database");
                return null;
            }

            try
            {

                using (SQLiteCommand fmd1 = connect.CreateCommand())
                {
                    fmd1.CommandText = @"SELECT * FROM FilesAsync";

                    
                    SQLiteDataReader reader = fmd1.ExecuteReader();
                    return reader;
                }

            }
            catch (SQLiteException ex)
            {
                MessageBox.Show("Error: " + ex.Message);
                return null;
            }
        }

        public SQLiteDataReader SqliteselectLastRows(SQLiteConnection connect)
        {
            if (connect.State != ConnectionState.Open)
            {
                MessageBox.Show("Open connection with database");
                return null;
            }

            try
            {

                using (SQLiteCommand fmd1 = connect.CreateCommand())
                {
                    fmd1.CommandText = @"  SELECT* FROM FilesAsync ORDER BY id DESC LIMIT 1;";


                    SQLiteDataReader reader = fmd1.ExecuteReader();
                    return reader;
                }

            }
            catch (SQLiteException ex)
            {
                MessageBox.Show("Error: " + ex.Message);
                return null;
            }
        }

    }
}
