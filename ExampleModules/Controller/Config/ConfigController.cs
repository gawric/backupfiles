﻿using DataModel.configModel;
using DataModel.statVariable;
using EventManager;
using ExampleModules.Controller.Config.support;
using ExampleModules.Controller.searchServerNetwork;
using ExampleModules.Controller.syncMode;
using ExampleModules.Modules.MainSupportModule;
using ExampleModules.Modules.SettingSupportModule;
using Microsoft.Practices.Unity;
using Prism.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ExampleModules.Controller.Config
{
    public class ConfigController
    {

        private loadConfig load;
        private saveConfig save;
        public iniSettingModel model;
        private checkConfigLoadAllRows checkConfig;
        private checkConfigLoadFiles checkLoad;
        private searchServer search;
        private IEventAggregator  m_eventAggregator;
        private syncModeController syncMode;
        private MainUpdateElements updateMain;
        private SettingUpdateElements updateView;
        //путь до файла c:/c#/program/setting.xml
        private string dir_setting = variable.root_folder_programm + "\\" + variable.config_root_file_name;

        public ConfigController(IUnityContainer container , IEventAggregator m_eventAggregator)
        {
            load = new loadConfig();
            save = new saveConfig();

            syncMode = container.Resolve<syncModeController>();
            checkConfig = container.Resolve<checkConfigLoadAllRows>();
            checkLoad = container.Resolve<checkConfigLoadFiles>();
            search = container.Resolve<searchServer>();
            updateMain = container.Resolve<MainUpdateElements>();
            updateView = container.Resolve<SettingUpdateElements>();
          
            this.m_eventAggregator = m_eventAggregator;




        }

        public iniSettingModel loadConfig()
        {
            model = load.Start(dir_setting);
            //Console.WriteLine("Ссылка на загружаемый конфиг "+dir_setting);
            return model;
            // Console.WriteLine(model.in_Dir);
        }

        public iniSettingModel getConfig()
        {
            if(model == null)
            {
                return model;
            }
            else
            {
                return model;
            }
            //return model;
        }

        //создает конфиг
        public string createConfig(string in_dir, string out_dir, string ip_statistics, string time, string select_sync)
        {
            Dictionary<string, string> array = new Dictionary<string, string>();

            string status;

            array["in_dir"] = in_dir;
            array["out_dir"] = out_dir;
            array["ip_statistics"] = ip_statistics;
            array["time"] = time;
            array["select_sync"] = select_sync;

            status = save.start(array, dir_setting);


            return status;
        }
        //обновляет конфиг
        public string saveConfig(string in_dir, string out_dir, string ip_statistics, string time, string select_sync)
        {
            Dictionary<string, string> array = new Dictionary<string, string>();
            Dictionary<string, string> DictionaryMyArray = new Dictionary<string, string>();
            string status;

            array["in_dir"] = in_dir;
            array["out_dir"] = out_dir;
            array["ip_statistics"] = ip_statistics;
            array["time"] = time;
            array["select_sync"] = select_sync;

            string[] myArrIpSection = ip_statistics.Split('.');

            for (int f = 0; f < myArrIpSection.Length; f++)
            {
                string item = myArrIpSection[f];
                DictionaryMyArray[item] = item;
            }



            if (checkString(array))
            {
                status = "ERROR: заполнены не все поля";
            }
            else if (ip_statistics.Equals("..."))
            {
                //Console.WriteLine("1");
                status = save.start(array, dir_setting);

                //поиск ip адреса сервера статистики
                search.Scan();
            }
            else
            {
                //отдельно разбиваем и проверяем ip на валидность
                if (checkString(DictionaryMyArray))
                {
                    status = "ERROR: Неправильный ip адрес";
                }
                else
                {
                    //Console.WriteLine("2");
                    status = save.start(array, dir_setting);

                    //останавливает поток для сканирования
                    search.getStop();

                }

            }

            return status;
        }

        //true - есть пустота
        private Boolean checkString(Dictionary<string, string> array)
        {
            Boolean check = false;

            foreach (var item in array)
            {
                if (String.IsNullOrEmpty(item.Value))
                {
                    check = true;
                }
            }

            return check;
        }

        //запускать после loadConfig
        public string checkLoadConfigController(iniSettingModel model)
        {
            return checkConfig.Start(model);
        }

        //запускать после loadConfig
        public Dictionary<string, iniSettingModel> checkLoadFilesConfig()
        {
            return checkLoad.checkLoadConfig(this);
        }
        //перенесен из главного окна т.к теперь view можно обновлять через интерфейс
        public void  CheckConfig()
        {
           
            Dictionary<string, iniSettingModel> status = checkLoadFilesConfig();

            // 0 - name elements
            // 1 - text
            string[] updateSettingView = new string[2];
            string[] updateMainView = new string[2];

            if (status.ContainsKey("OK") == true)
            {

                string elemetn = "labelInfoSettingStatus";
                string text = "Конф.загружен";

                updateView.UpdateSettingElemetns(elemetn, text);
                                    
                    //сама модель
                    iniSettingModel model = status["OK"];

                    string active_sync = model.active_sync;
                    string inDirFiles = model.in_Dir;
                    string outDirFIles = model.out_Dir;
                    string time = model.time_sync;
                    string ipStatis = model.ip_statistics;

                    //выбирает метод синхронизации
                    syncMode.selectSync(active_sync, inDirFiles , outDirFIles , time);

                    //добавляет какой флажок был выбран и вид синхронизации
                    //selectsync - по времени
                    //runtimeSync - по обновлению файла
                    if (active_sync.Equals(variable.select_sync))
                    {
                        updateView.UpdateSettingElemetns("updateSettingSelectSync", "true");
                    }
                    else
                    {
                        updateView.UpdateSettingElemetns("updateSettingSelectSync", "false");
                    }

                updateView.UpdateSettingElemetns("settingButtonPictureIn", inDirFiles);
                updateView.UpdateSettingElemetns("settingButtonPictureOut", outDirFIles);
                updateView.UpdateSettingElemetns("settingTextBoxIn", inDirFiles);
                updateView.UpdateSettingElemetns("settingTextBoxOut", outDirFIles);
                updateView.UpdateSettingElemetns("settingDateTime", time);

                updateMain.UpdateMainElemetns("visibleStatusLabel", "Работает");

                if (ipStatis.Equals("..."))
                {

                }
                else
                {
                   
                    updateView.UpdateSettingElemetns("textBoxIpStatistics", ipStatis);
                }



            }
                else if (status.ContainsKey("empty") == true)
                {
                    MessageBox.Show("Внимание Настройки не заполнены\nОстановка сервиса");

                updateView.UpdateSettingElemetns("labelInfoSettingStatus", "Конф.не заполнен");
                updateMain.UpdateMainElemetns("visibleStatusLabel" , "Остановлен");


                //какой вид синхронизации будем использовать
                //заглужка что-бы дать понять, что синхронизация не будет запущена
                //если конфиг пустой
                syncMode.noStart();

                }
                else
                {

                MessageBox.Show("Ошибка чтения конфига");


                syncMode.noStart();

                    createConfig("", "", "", "", "");
                    Environment.Exit(0);
                }
            }

        
    }
}
