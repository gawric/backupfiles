﻿using DataModel.configModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ExampleModules.Controller.Config.support
{
    class saveConfig
    {

        public string start(Dictionary<string, string> array, string dir_to_file)
        {
            string status;

            iniSettingModel iniSet = new iniSettingModel();

            iniSet.in_Dir = array["in_dir"];
            iniSet.out_Dir = array["out_dir"];
            iniSet.ip_statistics = array["ip_statistics"];
            iniSet.time_sync = array["time"];
            iniSet.active_sync = array["select_sync"];

            // выкидываем класс iniSet целиком в файл setting.xml
            using (Stream writer = new FileStream(dir_to_file, FileMode.Create))
            {

                try
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(iniSettingModel));
                    serializer.Serialize(writer, iniSet);
                    status = "OK";
                }
                catch (Exception ex)
                {
                    status = "ERROR: ошибка\n" + ex;
                }

            }

            return status;
        }
    }
}
