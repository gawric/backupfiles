﻿using DataModel.configModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExampleModules.Controller.Config.support
{
   public class checkConfigLoadAllRows
    {

        public string Start(iniSettingModel model_config)
        {
            string status;

            if (model_config != null)
            {
                int size = model_config.getArrayStrConf().Length;
                string[] arr = model_config.getArrayStrConf();

                Boolean check_null = false;
                Boolean check_empty = false;

                for (int s = 0; s < size; s++)
                {
                    if (arr[s] == null)
                    {
                        check_null = true;
                    }
                    else
                    {
                        if (arr[s].Equals(""))
                        {
                            check_empty = true;
                        }
                    }
                }

                if (check_null == true)
                {
                    //попытка создать конфиг файла заново
                    // MessageBox.Show("ERROR: не все параметры удалось получить\nПерезапустите приложение: попытка пересоздать setting.xml");
                    // createConfigSetting(cfg);
                    // Environment.Exit(0);

                    status = "ERROR: не все параметры удалось получить\nПерезапустите приложение: попытка пересоздать setting.xml";
                }
                else
                {
                    // labelInfoSettingStatus.Text = "Конфиг.Загружен";
                    // labelInfoSettingStatus.ForeColor = Color.Green;


                    if (check_empty)
                    {
                        status = "empty";
                    }
                    else
                    {
                        status = "OK";
                    }
                }


            }
            else
            {

                status = "ERROR: загрузить настройки не получилось \nПерезапустите приложение: попытка пересоздать setting.xml";
                //попытка создать конфиг файла заново
                // createConfigSetting(cfg);
                // Environment.Exit(0);
            }

            return status;
        }
    }
}
