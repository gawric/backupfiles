﻿using DataModel.configModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace ExampleModules.Controller.Config.support
{
    class loadConfig
    {
        public iniSettingModel Start(string dir)
        {
            iniSettingModel iniSet = null;

            if (File.Exists(dir))
            {
                iniSet = extractSetting(dir);
            }
            else
            {
                MessageBox.Show("ERROR: не найден setting.xml\nПерезапустите приложение: попытка пересоздать setting.xml");
            }

            return iniSet;

        }

        public iniSettingModel extractSetting(string dir)
        {
            iniSettingModel iniSet;

            try
            {
                // загружаем данные из файла program.xml 
                using (Stream stream = new FileStream(dir, FileMode.Open))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(iniSettingModel));
                    iniSet = (iniSettingModel)serializer.Deserialize(stream);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("ERROR: setting.xml не удалось получить настройки");
                Environment.Exit(0);
                throw new Exception(ex.ToString());

            }

            if (iniSet == null)
            {
                MessageBox.Show("ERROR: setting.xml не все данные получены");
                Environment.Exit(0);
            }

            return iniSet;

        }


    }
}
