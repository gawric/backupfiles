﻿using Prism.Events;
using Prism.Mvvm;
using Prism.Regions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExampleModules.ViewModel
{
    public class HelpViewModel
    {
        
            IEventAggregator m_eventAggregator;
            IRegionManager m_regionManager;

            public HelpViewModel(IEventAggregator eventAggregator, IRegionManager regionManager)
            {
                m_eventAggregator = eventAggregator;
                m_regionManager = regionManager;
            }
        
    }
}
