﻿using Microsoft.Practices.Unity;
using Prism.Commands;
using Prism.Events;
using Prism.Mvvm;
using Prism.Regions;
using System;
using System.Windows.Forms;

namespace ExampleModules.ViewModel
{
    public class SettingViewModel : BindableBase
    {

        IEventAggregator m_eventAggregator;
        IRegionManager m_regionManager;
      

        private bool _isEnabled;

        public SettingViewModel(IEventAggregator eventAggregator, IRegionManager regionManager)
        {
            m_eventAggregator = eventAggregator;
            m_regionManager = regionManager;
        }

        public bool isEnabled
        {
            get { return _isEnabled; }
            set
            {
                SetProperty(ref _isEnabled, value);
            }
        }

        

       
    }
}
