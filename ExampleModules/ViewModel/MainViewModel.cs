﻿using EventManager;
using ExampleModules.Modules.MainSupportModule;
using Microsoft.Practices.Unity;
using Prism.Events;
using Prism.Regions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExampleModules.ViewModel
{
    public class MainViewModel
    {
        IEventAggregator m_eventAggregator;
        IRegionManager m_regionManager;
        MainButton Mbutton;

        public MainViewModel(IEventAggregator eventAggregator, IRegionManager regionManager, IUnityContainer container)
        {
            m_eventAggregator = eventAggregator;
            m_regionManager = regionManager;
            Mbutton = container.Resolve<MainButton>();
          //  Subscrible();
        }

        
    }
}
